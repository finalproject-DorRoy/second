#pragma once
#ifndef DATABASE_H
#define DATABASE_H
#include "User.h"
#include "Question.h"
#include "sqlite3.h"
#include <map>
#include <exception>>
#include <unordered_map>
class DataBase {
	public:
		DataBase();
		~DataBase();
		bool isUserExists(string username);
		bool addNewUser(string username, string password, string email);
		bool isUserAndPasswordMatch(string username, string password);
		vector<Question*> initQuestions(int questionsNo);
		int insertNewGame();
		bool updateGameStatus(int status);
		bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
		void clearTable();
		int callback(void* notUsed, int argc, char** argv, char** azCol);
	private:
		sqlite3 *_db;
};
#endif