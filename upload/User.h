#pragma once
//#include "Room.h"
#include "Game.h"
#include "server.h"
#include <WinSock2.h>
#include <string>
using namespace std;

class User
{
private:
	string Username;
	Room* CurrRoom;
	Game* CurrGame;
	SOCKET sock;
public:
	User(string username,SOCKET socket);
	void send(string msg);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	void clearRoom();
	bool createRoom(int id, string name, int maxPlayers, int timePerQuestion,int numOfQuestions );
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
};