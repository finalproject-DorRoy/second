#include "User.h"

User::User(string username, SOCKET socket)
{
	this->Username = username;
	this->sock = socket;
	this->CurrGame = nullptr;
	this->CurrRoom = nullptr;
}

void User::send(string msg)
{
	;//??????????
}

string User::getUsername()
{
	return this->Username;
}

Game* User::getGame()
{
	return this->CurrGame;
}

SOCKET User::getSocket()
{
	return this->sock;
}

Room* User::getRoom()
{
	return this->CurrRoom;
}

void User::setGame(Game* game)
{
	this->CurrRoom = nullptr;
	this->CurrGame = game;
}

void User::clearRoom()
{
	this->CurrRoom = nullptr;
}

bool User::createRoom(int id, string name, int maxPlayers, int timePerQuestion, int numOfQuestions)
{
	if (this->CurrRoom)
	{
		send("1141");
		return false;
	}
	else
	{
		this->CurrRoom = new Room(id,this, name, maxPlayers, timePerQuestion, numOfQuestions);
		send("1140");
		return true;
	}
}

bool User::joinRoom(Room*room)
{
	Server* server = new Server();
	if (this->CurrRoom || !server->findRoom(room->getId()))
	{
		send("1102");
		return false;
	}
	else
	{
		room->JoinRoom(this);
		return true;//the send is already in the func room->JoinRoom()
	}
}

void User::leaveRoom()
{
	if (this->CurrRoom!=nullptr)
	{
		this->CurrRoom->leaveRoom(this);
	}
	this->CurrRoom = nullptr;
	send("112");
}

int User::closeRoom()
{
	if (this->CurrRoom == nullptr)
	{
		return -1;
	}

	int id = this->CurrRoom->closeRoom(this);
	if (id != -1)
	{
		this->CurrRoom = nullptr;
	}
	return id;
}