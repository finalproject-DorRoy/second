#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	if (password.length() < 4)
	{
		return false;
	}
	int count = 0;
	int conditions_true = 0;
	for (int i = 0; i < password.length(); i++)
	{
		if (password[i] == ' ')
		{
			return false;
		}
		if (isdigit(password[i]))
		{
			conditions_true++;
		}
		if (isupper(password[i]))
		{
			conditions_true++;
		}
		if (islower(password[i]))
		{
			conditions_true++;
		}
	}
	if (conditions_true == 3)
	{
		return true;
	}
	else
	{
		return false;
	}
}


bool Validator::isUsernameValid(string username)
{
	if (username.empty())
	{
		return false;
	}
	if (isalpha(username[0]))
	{
		return true;
	}
	for (int i = 0; i < username.length(); i++)
	{
		if (username[i] == ' ')
		{
			return false;
		}
	}
	return true;
}