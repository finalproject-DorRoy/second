#pragma once
#include <string>
using namespace std;
class Question
{
private:
	string question;
	string answers[4];
	int correctIndex;
	int Id;
public:
	Question(int id,string Question,string answer1, string answer2, string answer3, string correctAnswer);
	string getQuestion();
	string* getAnswers();
	int getCorrectIndex();
	int getId();
};