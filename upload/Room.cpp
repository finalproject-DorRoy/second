#include "Room.h"

Room::Room(int id, User* admin, string name, int num_of_questions, int time_per_qst, int maxplayers)
{
	this->Id = id;
	this->Admin = admin;
	this->Name = name;
	this->NumOfQuestions = num_of_questions;
	this->TimePerQuestion = time_per_qst;
	this->MaxPlayers = maxplayers;
	this->Users.push_back(admin);
}

bool Room::JoinRoom(User* user)
{
	if (this->Users.size()==this->MaxPlayers)
	{
		user->send("1101");
		return false;
	}
	else
	{
		this->Users.push_back(user);
		user->send("1100"+this->NumOfQuestions+this->TimePerQuestion);
		return true;
	}
}

void Room::leaveRoom(User* user)
{
	for (int i = 0; i < this->Users.size(); i++)
	{
		if (this->Users[i] == user)
		{
			this->Users.erase(this->Users.begin() + i);
		}
	}
}

int Room::closeRoom(User* user)
{
	if (user != this->Admin)
	{
		return -1;
	}
	for (int i = 0; i < this->Users.size(); i++)
	{
		this->Users[i]->send("116");
		if (this->Users[i] != this->Admin)
		{
			this->Users[i]->clearRoom();
		}
	}
	return this->Id;
}