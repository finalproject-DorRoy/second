#pragma once
#include "User.h"
#include <vector>
#include <string>
using namespace std;
class Room
{
private:
	vector<User*> Users;
	User* Admin;
	int MaxPlayers;
	int NumOfQuestions;
	int TimePerQuestion;
	string Name;
	int Id;

	string getUsersAsString(vector<User*> users, User* admin);
	void sendMessage(string msg);
	void sendMessage(User* user, string msg);
public:
	Room(int id, User* admin, string name, int num_of_questions, int time_per_qst, int maxplayers);
	bool JoinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	int getNumOfQuestions();
	string getUsersListMessages();
	int getId();
	string getName();
};