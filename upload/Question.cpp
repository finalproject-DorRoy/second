#include "Question.h"

Question::Question(int id, string Question, string answer1, string answer2, string answer3, string correctAnswer)
{
	this->Id = id;
	this->question = Question;
	int rnd = 0 + static_cast <int> (rand()) / (static_cast <int> (RAND_MAX / (3 - 0)));
	this->correctIndex = rnd;
	this->answers[rnd] = correctAnswer;
	if (rnd == 0)
	{
		this->answers[1] = answer1;
		this->answers[2] = answer2;
		this->answers[3] = answer3;
	}
	else
	{
		if (rnd == 1)
		{
			this->answers[0] = answer1;
			this->answers[2] = answer2;
			this->answers[3] = answer3;
		}
		else
		{
			if (rnd == 2)
			{
				this->answers[0] = answer1;
				this->answers[1] = answer2;
				this->answers[3] = answer3;
			}
			else
			{
				if (rnd == 3)
				{
					this->answers[0] = answer1;
					this->answers[1] = answer2;
					this->answers[2] = answer3;
				}
			}
		}
	}
}

string Question::getQuestion()
{
	return this->question;
}

string* Question::getAnswers()
{
	return this->answers;
}

int Question::getCorrectIndex()
{
	return this->correctIndex;
}

int Question::getId()
{
	return this->Id;
}