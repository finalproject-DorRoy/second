#include "DataBase.h"
#define DATABASE_NAME "ServerDataBase.db"


unordered_map<string, vector<string>> results;

DataBase::DataBase()
{
	char *zErrMsg = 0;
	int rc;
	rc = sqlite3_open(DATABASE_NAME, &this->_db);
	if (rc)
	{
		throw("database not opened");
	}
	rc = sqlite3_exec(this->_db, "CREATE TABLE Tuser(username text primarykey not null, password text not null, email text not null)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE Tgame(game_id integer primarykey autoincrement not null, status integer not null, start_time DATETIME not null, end_time DATETIME)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE Tquestion(question_id integer primary key autoincrement not null, question_text not null, correct_ans text not null, ans2 text not null, ans3 text not null, ans4 text not null, category text not null)", callback,0,&zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE Tanswers(game_id integer not null, username text not null, question_id integer not null, player_answer text not null, is_correct integer not null, answer_time integer not null, primary key(game_id,username,question_id), foreign key(game_id) REFERENCES Tgame(game_id), foreign key(username) REFERENCES Tuser(username), foreign key(question_id) REFERENCES Tquestion(question_id))", callback,0,&zErrMsg);
}

DataBase::~DataBase()
{
	sqlite3_close(this->_db);
}

int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i = 0;
	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	return 0;
}

void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

bool DataBase::isUserExists(string username)
{
	int rc;
	char *zErrMsg = 0;
	bool result;
	string message = "select * from Tuser where username = ";
	message = message + "\'" + username + "\'";
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end())
	{
		result = false;
	}
	else
	{
		result = true;
	}
	this->clearTable();
	return result;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	int rc;
	char *zErrMsg = 0;
	string message = "insert into Tuser (username, password, email) values(";
	message = message + "\'" + username + "\'";
	message = message + ", " + "\'" + password + "\'";
	message = message + ", " + "\'" + email + "\'" + ")";
	if (this->isUserExists(username))
	{
		return false;
	}
	rc = sqlite3_exec(this->_db, message.c_str(), callback, NULL, &zErrMsg);
	return true;
}

bool DataBase::isUserAndPasswordMatch(string username, string password)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select * from Tuser where username = ";
	message = message + "\'" + username + "\'";
	message = message + " and password = ";
	message = message + "\'" + password + "\'";
	bool result;

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end())
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;

}

vector<Question*> DataBase::initQuestions(int questionNo)
{
	int rc;
	char *zErrMsg = 0;
	vector<Question*> questions;
	int randomId;
	const int temp = questionNo;
	bool* checkIfChosen;
	string message;
	int amountOfQuestionsId;
	int id;
	string question;
	string correct_answer;
	string ans2;
	string ans3;
	string ans4;

	rc = sqlite3_exec(this->_db, "select * from t_questions", callback, 0, &zErrMsg);
	amountOfQuestionsId = results.find("question_id")->second.size();
	checkIfChosen = new bool[amountOfQuestionsId];

	if (results.find("question_id")->second.size() < questionNo)
	{
		throw("too much questions have been chosen");
	}
	else
	{
		for (int i = 0; i < amountOfQuestionsId; i++)
		{
			checkIfChosen[i] = false;
		}
		for (int j = 0; j < questionNo; j++)
		{
			randomId = std::rand() % amountOfQuestionsId + 1;
			while (checkIfChosen[randomId - 1])
			{
				randomId = std::rand() % amountOfQuestionsId + 1;
			}
			checkIfChosen[randomId - 1] = true;
			message = "select question_id, question, correct_ans, ans2,ans3,ans4 from Tquestion limit" + std::to_string(randomId - 1) + ", 1";
			this->clearTable();
			rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

			for (auto it = results.begin(); it != results.end(); it++)
			{
				if (it->first._Equal(question))
				{
					id = atoi(it->second[0].c_str());
				}
				else if (it->first._Equal(correct_answer))
				{
					correct_answer = it->second[0];
				}
				else if (it->first._Equal(ans2))
				{
					ans2 = it->second[0];
				}
				else if (it->first._Equal(ans3))
				{
					ans3 = it->second[0];
				}
				else
				{
					ans4 = it->second[0];
				}
			}

			Question* qst = new Question(id, question, correct_answer, ans2, ans3, ans4);
			questions.push_back(qst);
			this->clearTable();
		}
	}
	return questions;
}

int DataBase::insertNewGame()
{
	int rc;
	char *zErrMsg = 0;
	int id;

	//create the game
	rc = sqlite3_exec(this->_db, "insert into Tgame (status, start_time, end_time) values(0, datetime('NOW'), datetime('NOW'))", callback, NULL, &zErrMsg);
	this->clearTable();
	rc = sqlite3_exec(this->_db, "select game_id from Tgames where game_id = last_insert_rowid()", callback, 0, &zErrMsg);
	id = atoi(results.begin()->second[0].c_str());
	this->clearTable();
	return id;
}

bool DataBase::updateGameStatus(int gameId)
{
	int rc;
	char *zErrMsg = 0;
	string message = "update Tgame set status = 1, end_time = 'NOW' where gema_id = " + std::to_string(gameId);

	try
	{
		rc = sqlite3_exec(this->_db, message.c_str(), callback, NULL, &zErrMsg);
		this->clearTable();
		return true;
	}
	catch(...) {}
	return false;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	int rc;
	char *zErrMsg = 0;
	string message = "insert into Tanswers (game_id, username, question_id, player_answer, is_correct, answer_time) values(";
	message = message + std::to_string(gameId) + ", ";
	message = message + "\'" + username + "\'" + ", ";
	message = message + std::to_string(questionId) + ", ";
	message = message + "\'" + answer + "\'" + ", ";
	if (isCorrect)
	{
		message = message + "1" + ", " + std::to_string(answerTime) + ")";
	}
	else
	{
		message = message + "0" + ", " + std::to_string(answerTime) + ")";
	}
	try
	{
		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
	}
	catch(...){}
	return false;
}

